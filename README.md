# CI demo

I created a badge for reference to 1.e in Assignment 5

![Pipeline Status](https://gitlab.com/hrafnhilduro/cidemo2020/-/blob/master/Badge.JPG)

I created a badge for reference to 1.f in Assignment 5

![Coverage Status for Test and Build](https://gitlab.com/hrafnhilduro/cidemo2020/-/blob/master/badge2.JPG)
